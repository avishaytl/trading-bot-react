import React from 'react';
import '../style/InputApp.css';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
export default class AutocompleteApp extends React.Component{
    constructor(props){
        super(props);
        this.state ={
            id: '',
        }
    } 
    render(){
        const capitalize = (s) => {
            if (typeof s !== 'string') return ''
            return s.charAt(0).toUpperCase() + s.slice(1)
          }
        return(
            <div className='input-app'>
                <div className='input-app-inside'>
                    {this.props.icon}
                    <Autocomplete 
                        options={this.props.options} 
                        getOptionLabel={option => capitalize(option.name)} 
                        onChange = {(e) => this.props.onChange(e,this.props.label)} 
                        style={{ width: this.props.label === 'Select Club To Send' ? 170 : '100%'}}   
                        renderInput={params => (
                            <TextField {...params}  InputLabelProps={{style: {color: this.props.labelColor,fontWeight: 'bold'}}}  label={this.props.label} fullWidth />
                        )}
                        />
                </div>
            </div>
        );
    }
}