import React from 'react';
import '../style/InputApp.css';
import TextField from '@material-ui/core/TextField';
export default class Input extends React.Component{
    constructor(props){
        super(props);
        this.state ={
            id: '',
        }
    }
    render(){
        return(
            <div className='input-app'>
                <div className='input-app-inside'>
                    {this.props.icon} 
                    <TextField   
                        style={{margin:0}}
                        type={this.props.type}
                        InputLabelProps={{style: {color: this.props.labelColor,fontWeight: 'bold'}}}
                        InputProps={{style: {color: this.props.inputColor}}} 
                        defaultValue={this.props.defaultValue} 
                        onChange={(e) => this.props.onChange(e,this.props.label)} 
                        label={this.props.label} margin="normal" fullWidth />   
                </div>
            </div>
        );
    }
}