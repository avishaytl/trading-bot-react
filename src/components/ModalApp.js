import React from 'react'; 
import Loader from './loader.js'; 
import primarypeme from '../style/style'; 
export default class ModalApp extends React.Component{
    constructor(props){
        super(props);
        this.state ={
            id: '',
        }
    }
    render(){
        switch(this.props.name){
            case 'loading':
            return( 
                <div style={primarypeme.modal_loading}>
                    <label style={{position:'absolute',top:70,fontSize:14}}>{this.props.title_name}</label>
                    <Loader />
                </div>
            ) 
            default:
                return(
                    <div style={primarypeme.modal_default_out_view}>
                        <div onClick={()=>this.props.func()} style={primarypeme.modal_default_inside_view}>
                        </div>
                        <div style={primarypeme.modal_loading}> 
                            <label style={{position:'absolute' ,fontSize:14,padding:10,textAlign:'center'}}>{this.props.title_name}</label>
                        </div> 
                    </div>
                )
        }
    }
}