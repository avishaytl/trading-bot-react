import React from 'react'; 
export default class AlertBox extends React.Component{
    constructor(props){
        super(props);
        this.state ={
            id: '',
        }
    }
    render(){
        if(this.props.data)
            return(
                <div className="alert-box-inside" > 
                    <p className='text' style={{textAlign:'center',color:'white'}}>{this.props.title}</p> 
                    <p className='text'>symbol: <b style={{color:'yellow'}}>{this.props.data.symbol}</b></p>
                    <p className='text'>price: <b style={{color:'red'}}>{this.props.data.price}</b></p>
                    <p className='text'>high: <b style={{color:'white'}}>{this.props.data.high}</b></p>
                    <p className='text'>low: <b style={{color:'white'}}>{this.props.data.low}</b></p>
                    <p className='text'>close: <b style={{color:'white'}}>{this.props.data.close}</b></p>
                    <p className='text'>volume: <b style={{color:'white'}}>{this.props.data.volume}</b></p>
                    <p className='text'>latest trading day: <b style={{color:'white'}}>{this.props.data.latest_trading_day}</b></p>
                    <p className='text'>change: <b style={{color:'white'}}>{this.props.data.change}</b></p>
                    <p className='text'>change percent: <b style={{color:'white'}}>{this.props.data.change_percent}</b></p> 
                </div>
            );
        else
        return(
            <div className="alert-box">  
            </div>
        );
    }
}