import primaryValues from '../constants/Values.js';
const initialState = {
  user_email: null, 
  login_success: false,
  loading_login: false, 
  error_msg: '', 
  col1: null,
  col2: null,
  col3: null,
  col4: null,
  col5: null,
  col1Set: false,
  col2Set: false,
  col3Set: false,
  col4Set: false,
  col5Set: false,
  alphaData: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case primaryValues.$GET_LOGIN:
      return{
        ...state,
        error_msg: '',
        loading_login: true,
      }
    case primaryValues.$LOGIN_RESULT:
      return{
        ...state,
        user_email: action.response.user.email,
        user_token: action.response.user.remember_token, 
        login_success: true,
        loading_login: false,
      }
    case primaryValues.$LOGIN_ERROR:
      return{
        ...state,
        error_msg: action.response.msg,
        login_success: false,
        loading_login: false,
      }  
    case primaryValues.$ALPHA_RESULT_COL1:
      return{
        ...state, 
        col1: action.data,
        col1Set: true,
        col2Set: false,
        col3Set: false,
        col4Set: false,
        col5Set: false,
        alphaData: true,
      }
    case primaryValues.$ALPHA_RESULT_COL2:
      return{
        ...state, 
        col2: action.data,
        col1Set: false,
        col2Set: true,
        col3Set: false,
        col4Set: false,
        col5Set: false,
        alphaData: true,
      }
    case primaryValues.$ALPHA_RESULT_COL3:
      return{
        ...state, 
        col3: action.data,
        col1Set: false,
        col2Set: false,
        col3Set: true,
        col4Set: false,
        col5Set: false,
        alphaData: true,
      }
    case primaryValues.$ALPHA_RESULT_COL4:
      return{
        ...state, 
        col4: action.data,
        col1Set: false,
        col2Set: false,
        col3Set: false,
        col4Set: true,
        col5Set: false,
        alphaData: true,
      }
    case primaryValues.$ALPHA_RESULT_COL5:
      return{
        ...state, 
        col5: action.data,
        col1Set: false,
        col2Set: false,
        col3Set: false,
        col4Set: false,
        col5Set: true,
        alphaData: true,
      }
    case primaryValues.$ALPHA_ERROR: 
      return{
        ...state, 
        // alphaData: false,
      }
    default:
      return state;
  }
};

export default reducer;