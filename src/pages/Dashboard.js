import React from 'react';
import '../App.css';
import '../style/Dashboard.css';
import AlertBox from '../components/AlertBox.js';
import * as IconsFa from "react-icons/fa";
import * as IconsGi from "react-icons/gi";
import { connect } from "react-redux";
import primaryValues from '../constants/Values.js'; 
import { Scrollbars } from 'react-custom-scrollbars';
import FlatList from 'flatlist-react';
import Chart from '../components/Chart';  
import anime from 'animejs/lib/anime.es.js';
class Dashboard extends React.Component{
  constructor(props){
    super(props);
    let date = new Date(); 
    this.state={
      id: '', 
      today: date,
      countRefresh: 0,
      params: [
        {
          pos: 1,
          function: 'GLOBAL_QUOTE',
          symbol: 'BNTX',
          interval: '1min',
        },
        {
          pos: 2,
          function: 'GLOBAL_QUOTE',
          symbol: 'AMZN',
          interval: '1min',
        },
        {
          pos: 3,
          function: 'GLOBAL_QUOTE',
          symbol: 'CLX',
          interval: '1min',
        },
        {
          pos: 4,
          function: 'GLOBAL_QUOTE',
          symbol: 'SJM',
          interval: '1min',
        },
        {
          pos: 5,
          function: 'GLOBAL_QUOTE',
          symbol: 'USD',
          interval: '1min',
        },
      ],
      chartColumnsData:{
        col1:[],
        col2:[],
        col3:[],
        col4:[],
        col5:[],
      },
      changePositionData:{
        col1:[{pos:'start',icon: <IconsFa.FaExpandArrowsAlt style={{color:'blue'}}/>,time: date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds()}],
        col2:[{pos:'start',icon: <IconsFa.FaExpandArrowsAlt style={{color:'blue'}}/>,time: date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds()}],
        col3:[{pos:'start',icon: <IconsFa.FaExpandArrowsAlt style={{color:'blue'}}/>,time: date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds()}],
        col4:[{pos:'start',icon: <IconsFa.FaExpandArrowsAlt style={{color:'blue'}}/>,time: date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds()}],
        col5:[{pos:'start',icon: <IconsFa.FaExpandArrowsAlt style={{color:'blue'}}/>,time: date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds()}],
      }, 
      lastChangesData:{
        col1: null,
        col2: null,
        col3: null,
        col4: null,
        col5: null,
      }, 
      checkChangePosition:{
        col1:[null,null,null],
        col2:[null,null,null],
        col3:[null,null,null],
        col4:[null,null,null],
        col5:[null,null,null],
      },
    } 
    let params = this.state.params[0];
    this.props.fetchAlphaData({ type: primaryValues.$GET_ALPHA, params});  
    setTimeout(() => {
      params = this.state.params[1];
      this.props.fetchAlphaData({ type: primaryValues.$GET_ALPHA, params});  
    }, 5000); 
    setTimeout(() => {
      params = this.state.params[2];
      this.props.fetchAlphaData({ type: primaryValues.$GET_ALPHA, params});  
    }, 10000);
    setTimeout(() => {
      params = this.state.params[3];
      this.props.fetchAlphaData({ type: primaryValues.$GET_ALPHA, params});  
    }, 15000);
    setTimeout(() => {
      params = this.state.params[4];
      this.props.fetchAlphaData({ type: primaryValues.$GET_ALPHA, params});  
    }, 20000);
    this.setChartColData = this.setChartColData.bind(this);  
    this.setStateColData = this.setStateColData.bind(this);  
    this.setChangeStateColData = this.setChangeStateColData.bind(this);  
    this.checkChangesPosition = this.checkChangesPosition.bind(this);   
  } 
  setChartColData = (pos, params) => {
    let data = this.state.chartColumnsData;
    switch(pos){
      case 'col1':
        data.col1.push(params);
        break;
      case 'col2':
        data.col2.push(params);
        break;
      case 'col3':
        data.col3.push(params);
        break;
      case 'col4':
        data.col4.push(params);
        break;
      case 'col5':
        data.col5.push(params);
        break;
    } 
    this.setState({chartColumnsData: data});  
  } 
  UNSAFE_componentWillMount(){
    let params;
    setInterval(() => {
        console.log('refresh'); 
        this.setState({countRefresh: this.state.countRefresh + 1});
        params = this.state.params[0];
        this.props.fetchAlphaData({ type: primaryValues.$GET_ALPHA, params});  
      setTimeout(() => {
        params = this.state.params[1];
        this.props.fetchAlphaData({ type: primaryValues.$GET_ALPHA, params});  
      }, 5000); 
      setTimeout(() => {
        params = this.state.params[2];
        this.props.fetchAlphaData({ type: primaryValues.$GET_ALPHA, params});  
      }, 10000);
      setTimeout(() => {
        params = this.state.params[3];
        this.props.fetchAlphaData({ type: primaryValues.$GET_ALPHA, params});  
      }, 15000);
      setTimeout(() => {
        params = this.state.params[4];
        this.props.fetchAlphaData({ type: primaryValues.$GET_ALPHA, params});  
      }, 20000);
    }, 60000);
  } 
  setStateColData = (pos,val) =>{ 
    switch(pos){
      case 'col1': 
        this.setState({lastChangesData: {...this.state.lastChangesData,col1: val }}); 
        break;
      case 'col2': 
        this.setState({lastChangesData: {...this.state.lastChangesData,col2: val }}); 
        break;
      case 'col3': 
        this.setState({lastChangesData: {...this.state.lastChangesData,col3: val }}); 
        break;
      case 'col4': 
        this.setState({lastChangesData: {...this.state.lastChangesData,col4: val }}); 
        break;
      case 'col5': 
        this.setState({lastChangesData: {...this.state.lastChangesData,col5: val }}); 
        break;
    } 
  }
  setChangeStateColData = (pos,array) =>{   
    switch(pos){
      case 'col1':   
        this.setState({changePositionData: {...this.state.changePositionData,col1: array }});  
        break;
      case 'col2':  
        this.setState({changePositionData: {...this.state.changePositionData,col2: array }});   
        break;
      case 'col3':  
        this.setState({changePositionData: {...this.state.changePositionData,col3: array }});  
        break;
      case 'col4':  
        this.setState({changePositionData: {...this.state.changePositionData,col4: array }});   
        break;
      case 'col5':  
        this.setState({changePositionData: {...this.state.changePositionData,col5: array }});  
        break;
    }  
  }
  checkPosition = (change1,change2) => {
    change1 = parseFloat(change1);
    change2 = parseFloat(change2);
    console.log('change1',change1,'change2',change2);
    return change1 === change2 ? {pos:'static',icon:<IconsFa.FaArrowsAltH/>} : change1 >= change2 ? {pos:'up',icon:<IconsFa.FaLongArrowAltUp/>} : {pos:'down',icon:<IconsFa.FaLongArrowAltDown/>};
  }
  changeIndex = (changeArray,change) =>{
    console.log('changeArray',changeArray);
    if(!changeArray[0]) {
      changeArray[0] = change;
      return changeArray;
    }
    else{
      if(changeArray[0] === change) {
        if(changeArray[1] === change){
          changeArray[2] = change;
          return true;
        }else{ 
          changeArray[1] = change;
          return changeArray;
        }
      }
      else{
        if(changeArray[0] !== change){
          changeArray[0] = change;
          changeArray[1] = null;
          return changeArray;
        }
      } 
    }
  }
  checkChangesPosition = (pos,val) =>{
    console.log('checkChangesPosition',pos);
    let index;
    switch(pos){
      case 'col1': 
        index = this.changeIndex(this.state.checkChangePosition.col1,val);
        if(index !== true){
          this.setState({checkChangePosition: {...this.state.checkChangePosition,col1: index}});
          return false;
        } else{ 
          this.setState({checkChangePosition: {...this.state.checkChangePosition,col1: [null,null,null]}});
          return true;
        } 
        break;
      case 'col2':  
        index = this.changeIndex(this.state.checkChangePosition.col2,val);
        if(index !== true){
          this.setState({checkChangePosition: {...this.state.checkChangePosition,col2: index}});
          return false;
        } else{ 
          this.setState({checkChangePosition: {...this.state.checkChangePosition,col2: [null,null,null]}});
          return true;
        } 
        break;
      case 'col3': 
        index = this.changeIndex(this.state.checkChangePosition.col3,val);
        if(index !== true){
          this.setState({checkChangePosition: {...this.state.checkChangePosition,col3: index}});
          return false;
        } else{ 
          this.setState({checkChangePosition: {...this.state.checkChangePosition,col3: [null,null,null]}});
          return true;
        } 
        break;
      case 'col4':  
        index = this.changeIndex(this.state.checkChangePosition.col4,val);
        if(index !== true){
          this.setState({checkChangePosition: {...this.state.checkChangePosition,col4: index}});
          return false;
        } else{ 
          this.setState({checkChangePosition: {...this.state.checkChangePosition,col4: [null,null,null]}});
          return true;
        } 
        break;
      case 'col5':  
        index = this.changeIndex(this.state.checkChangePosition.col5,val);
        if(index !== true){
          this.setState({checkChangePosition: {...this.state.checkChangePosition,col5: index}});
          return false;
        } else{ 
          this.setState({checkChangePosition: {...this.state.checkChangePosition,col5: [null,null,null]}});
          return true;
        } 
        break;
    }  
  }
  UNSAFE_componentWillReceiveProps(){
    let data,open,high,low,close; 
    let arrayCanagesData = [];
    let check = [];
    let date = new Date();
    let pos, icon, time;   
    if(this.props.initialState.col1Set){ 
      data = this.props.initialState.col1; 
      open = parseFloat(data.open); high = parseFloat(data.high); low = parseFloat(data.low); close = parseFloat(data.close);
      this.setChartColData('col1' ,{ x: new Date(), y: [open, high, low, close] }); 
      if(this.state.lastChangesData.col1 === null){
        this.setStateColData('col1',data.change);  
      }else{   
        arrayCanagesData = this.state.changePositionData.col1;
        check = this.checkPosition(data.change,this.state.lastChangesData.col1);
        pos = check.pos; icon = check.icon; time = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds(); 
        if(this.checkChangesPosition('col1',pos)){
          anime({
            targets: '#match-box-col1', 
            opacity: 1,   
            duration: 4000, 
          }); 
          anime({
            targets: '#match-box-col1-icon', 
            scale: 2,   
            duration: 1000, 
            loop:true
          });   
        }else{ 
          anime({
            targets: '#match-box-col1', 
            opacity: 0,   
            duration: 4000,
          });   
        }
        arrayCanagesData.push({pos,icon,time}); 
        this.setChangeStateColData('col1',arrayCanagesData); 
      } 
    }
    if(this.props.initialState.col2Set){ 
      data = this.props.initialState.col2;
      open = parseFloat(data.open); high = parseFloat(data.high); low = parseFloat(data.low); close = parseFloat(data.close); 
      this.setChartColData('col2' ,{ x: new Date(), y: [open, high, low, close] }); 
      if(this.state.lastChangesData.col2 === null){
        this.setStateColData('col2',data.change);  
      }else{   
        arrayCanagesData = this.state.changePositionData.col2;
        check = this.checkPosition(data.change,this.state.lastChangesData.col2);
        pos = check.pos; icon = check.icon; time = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
        if(this.checkChangesPosition('col2',pos)){
          anime({
            targets: '#match-box-col2', 
            opacity: 1,   
            duration: 4000, 
          });    
          anime({
            targets: '#match-box-col2-icon', 
            scale: 2,   
            duration: 1000, 
            loop:true
          });  
        }else{
          anime({
            targets: '#match-box-col2', 
            opacity: 0,   
            duration: 4000,
          });  
        }
        arrayCanagesData.push({pos,icon,time}); 
        this.setChangeStateColData('col2',arrayCanagesData); 
      } 
    }
    if(this.props.initialState.col3Set){ 
      data = this.props.initialState.col3;
      open = parseFloat(data.open); high = parseFloat(data.high); low = parseFloat(data.low); close = parseFloat(data.close);
      this.setChartColData('col3' ,{ x: new Date(), y: [open, high, low, close] }); 
      if(this.state.lastChangesData.col3 === null){
        this.setStateColData('col3',data.change);  
      }else{   
        arrayCanagesData = this.state.changePositionData.col3;
        check = this.checkPosition(data.change,this.state.lastChangesData.col3);
        pos = check.pos; icon = check.icon; time = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
        if(this.checkChangesPosition('col3',pos)){
          anime({
            targets: '#match-box-col3', 
            opacity: 1,   
            duration: 4000, 
          });   
          anime({
            targets: '#match-box-col3-icon', 
            scale: 2,   
            duration: 1000, 
            loop:true
          });  
        }else{
          anime({
            targets: '#match-box-col3', 
            opacity: 0,   
            duration: 4000,
          });  
        }
        arrayCanagesData.push({pos,icon,time}); 
        this.setChangeStateColData('col3',arrayCanagesData); 
      } 
    }
    if(this.props.initialState.col4Set){ 
      data = this.props.initialState.col4;
      open = parseFloat(data.open); high = parseFloat(data.high); low = parseFloat(data.low); close = parseFloat(data.close); 
      this.setChartColData('col4' ,{ x: new Date(), y: [open, high, low, close] }); 
      if(this.state.lastChangesData.col4 === null){
        this.setStateColData('col4',data.change);  
      }else{   
        arrayCanagesData = this.state.changePositionData.col4;
        check = this.checkPosition(data.change,this.state.lastChangesData.col4);
        pos = check.pos; icon = check.icon; time = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
        if(this.checkChangesPosition('col4',pos)){
          anime({
            targets: '#match-box-col4', 
            opacity: 1,   
            duration: 4000, 
          });    
          anime({
            targets: '#match-box-col4-icon', 
            scale: 2,   
            duration: 1000, 
            loop:true
          });  
        }else{
          anime({
            targets: '#match-box-col4', 
            opacity: 0,   
            duration: 4000,
          });  
        }
        arrayCanagesData.push({pos,icon,time}); 
        this.setChangeStateColData('col4',arrayCanagesData); 
      } 
    }
    if(this.props.initialState.col5Set){ 
      data = this.props.initialState.col5;
      open = parseFloat(data.open); high = parseFloat(data.high); low = parseFloat(data.low); close = parseFloat(data.close); 
      this.setChartColData('col5' ,{ x: new Date(), y: [open, high, low, close] }); 
      if(this.state.lastChangesData.col5 === null){
        this.setStateColData('col5',data.change);  
      }else{   
        arrayCanagesData = this.state.changePositionData.col5;
        check = this.checkPosition(data.change,this.state.lastChangesData.col5);
        pos = check.pos; icon = check.icon; time = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
        if(this.checkChangesPosition('col5',pos)){
          anime({
            targets: '#match-box-col5', 
            opacity: 1,   
            duration: 4000, 
          });    
          anime({
            targets: '#match-box-col5-icon', 
            scale: 2,   
            duration: 1000, 
            loop:true
          });  
        }else{
          anime({
            targets: '#match-box-col5', 
            opacity: 0,   
            duration: 4000,
          });  
        }
        arrayCanagesData.push({pos,icon,time}); 
        this.setChangeStateColData('col5',arrayCanagesData); 
      } 
    }
  }
  renderChangeListItem = (item, idx) => {
    return (
        <div className='item-pos' key={`${item.time}-${idx}`}>
          <div className='item-pos-left'>
            <i style={{color: item.pos === 'up' ? 'green' : item.pos === 'down' ? 'red' : 'yellow',paddingTop:5}}>{item.icon}</i>
            <p style={{marginLeft:5}}>{item.pos}</p>
          </div>
          <div className='item-pos-right'>
            <p>{item.time}</p>
          </div>
        </div>
    );
  }
  render(){ 
      return (
        <div className='App'> 
          <div className='stats-box'>
            <p style={{color:'white',fontSize:12}}>count refresh time/minute: {this.state.countRefresh}</p>
            <p style={{color:'white',fontSize:12}}>{this.state.today.toString()}</p>
          </div>
          {this.props.initialState.alphaData === true ? 
            <div className='dashboard'> 
                <div className="alert-box"> 
                    <AlertBox data={this.props.initialState.col1} title={this.state.params[0].function}/> 
                    {this.props.initialState.col1 ? <Chart data={this.state.chartColumnsData.col1} time={''} symbol={this.state.params[0].symbol} /> : null }
                    <Scrollbars className='flatlist-box' style={{height:100}}>
                      <FlatList list={this.state.changePositionData.col1} renderItem={(item)=>this.renderChangeListItem(item)}/>
                    </Scrollbars>
                    <div className='match-box' id='match-box-col1'>
                        <p>find in {this.state.checkChangePosition.col1.length} minues</p>
                        <div id='match-box-col1-icon'>
                          <IconsGi.GiTrefoilShuriken style={{fontSize:20,paddingBottom:2,color:'yellow'}}/>
                        </div>
                    </div>
                </div>
                <div className="alert-box"> 
                    <AlertBox data={this.props.initialState.col2} title={this.state.params[1].function}/> 
                    {this.props.initialState.col2 ? <Chart data={this.state.chartColumnsData.col2} time={''} symbol={this.state.params[1].symbol} /> : null }
                    <Scrollbars className='flatlist-box' style={{height:100}}>
                      <FlatList list={this.state.changePositionData.col2} renderItem={(item)=>this.renderChangeListItem(item)}/>
                    </Scrollbars>
                    <div className='match-box' id='match-box-col2'>
                        <p>find in {this.state.checkChangePosition.col2.length} minues</p>
                        <div id='match-box-col2-icon'>
                          <IconsGi.GiTrefoilShuriken style={{fontSize:20,paddingBottom:2,color:'yellow'}}/>
                        </div>
                    </div>
                </div>
                <div className="alert-box"> 
                    <AlertBox data={this.props.initialState.col3} title={this.state.params[2].function}/> 
                    {this.props.initialState.col3 ? <Chart data={this.state.chartColumnsData.col3} time={''} symbol={this.state.params[2].symbol} /> : null }
                    <Scrollbars className='flatlist-box' style={{height:100}}>
                      <FlatList list={this.state.changePositionData.col3} renderItem={(item)=>this.renderChangeListItem(item)}/>
                    </Scrollbars>
                    <div className='match-box' id='match-box-col3'>
                        <p>find in {this.state.checkChangePosition.col3.length} minues</p>
                        <div id='match-box-col3-icon'>
                          <IconsGi.GiTrefoilShuriken style={{fontSize:20,paddingBottom:2,color:'yellow'}}/>
                        </div>
                    </div>
                </div>
                <div className="alert-box"> 
                    <AlertBox data={this.props.initialState.col4} title={this.state.params[3].function}/> 
                    {this.props.initialState.col4 ? <Chart data={this.state.chartColumnsData.col4} time={''} symbol={this.state.params[3].symbol} /> : null }
                    <Scrollbars className='flatlist-box' style={{height:100}}>
                      <FlatList list={this.state.changePositionData.col4} renderItem={(item)=>this.renderChangeListItem(item)}/>
                    </Scrollbars>
                    <div className='match-box' id='match-box-col4'>
                        <p>find in {this.state.checkChangePosition.col4.length} minues</p>
                        <div id='match-box-col4-icon'>
                          <IconsGi.GiTrefoilShuriken style={{fontSize:20,paddingBottom:2,color:'yellow'}}/>
                        </div>
                    </div>
                </div>
                <div className="alert-box"> 
                    <AlertBox data={this.props.initialState.col5} title={this.state.params[4].function}/> 
                    {this.props.initialState.col5 ? <Chart data={this.state.chartColumnsData.col5} time={''} symbol={this.state.params[4].symbol} /> : null }
                    <Scrollbars className='flatlist-box' style={{height:100}}>
                      <FlatList list={this.state.changePositionData.col5} renderItem={(item)=>this.renderChangeListItem(item)}/>
                    </Scrollbars>
                    <div className='match-box' id='match-box-col5'>
                        <p>find in {this.state.checkChangePosition.col5.length} minues</p>
                        <div id='match-box-col5-icon'>
                          <IconsGi.GiTrefoilShuriken style={{fontSize:20,paddingBottom:2,color:'yellow'}}/>
                        </div>
                    </div>
                </div>
            </div>
          : <p style={{color:'white',fontSize:12}}>trying to get the system, this process can take up to a minute...</p>}
        {/* <p style={{position:'absolute',bottom:0,right:0,color:'white',fontSize:12}}>Avishay Tal - avishaytl@gmail.com</p> */}
        </div>
      )
  }
}

const mapStateToProps = state => {
  return {
    initialState: state.initialState,
  };
};
const mapDispachToProps = dispatch => {
  return {
    fetchAlphaData: (info) => dispatch(info),
  };
};
export default connect(mapStateToProps,mapDispachToProps)(Dashboard);