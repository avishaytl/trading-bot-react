import React from 'react';
import logo from '../logo.svg';
import { FaUserTag, FaUserLock } from 'react-icons/fa';
import '../App.css';
import Input from '../components/InputApp.js';
import Button from '../components/ButtonApp.js';
import Modal from '../components/ModalApp.js';
import primaryValues from '../constants/Values.js';
import { connect } from "react-redux";
import { Redirect } from 'react-router'; 
class Login extends React.Component{
  constructor(props){
    super(props);
    this.state={
      email: '',
      password: '',
    }
    this.updateInputEmail = this.updateInputEmail.bind(this);
    this.updateInputPassword = this.updateInputPassword.bind(this);
  }
  updateInputEmail = (event,name) => {
    this.setState({email : event.target.value});
  }
  updateInputPassword = (event,name) => {
    this.setState({password : event.target.value});
  }
  fetchLogin = () =>{
    let params = {
      email: this.state.email,
      password: this.state.password,
    }
    this.props.fetchLogin({ type: primaryValues.$GET_LOGIN, params });
  }
  render(){
    if (this.props.initialState.login_success) {
      return <Redirect to={{pathname: '/Dashboard'}}/>;
    }
    return (
      <div className="App">
          <div className="main-view"> 
            {this.props.initialState.loading_login === true ?  <Modal name={'loading'} title_name={'Loading...'}/>: null}
            <p className='title-text-app'>Trading Bot</p>
            <Input icon={<FaUserTag style={{color:'#2d2d2d',fontSize:24,padding:0,margin:0,marginRight:5 }}/>} inputColor={'#e9e9e9'} labelColor={'#2d2d2d'} defaultValue={''} label={'Email'} type={'text'} onChange={this.updateInputEmail} />
            <Input icon={<FaUserLock style={{color:'#2d2d2d',fontSize:24,padding:0,margin:0,marginRight:5 }}/>} inputColor={'#e9e9e9'} labelColor={'#2d2d2d'} defaultValue={''} label={'Password'} type={'password'} onChange={this.updateInputPassword}/>
            <p className='error-text-app' style={{padding:'0px',margin:'0px',fontSize:'13px',fontWeight:'bold'}}>{this.props.initialState.error_msg}</p>
            <Button onClick={this.fetchLogin} name={'Sign In'}/>
          </div> 
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    initialState: state.initialState,
  };
};
const mapDispachToProps = dispatch => {
  return {
    fetchLogin: (info) => dispatch(info)
  };
};
export default connect(mapStateToProps,mapDispachToProps)(Login);