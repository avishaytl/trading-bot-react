
export const primaryTheme = {
    modal_loading:{
        position:'absolute',
        width: 300,
        height: 200,
        borderRadius: 5,
        backgroundColor: '#181818',
        zIndex: 1000,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center', 
        paddingBottom:25,
    },
    modal_default:{  
        backgroundColor: '#181818',
        position: 'absolute',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'center',
        padding:10, 
        fontSize: 14,
        zIndex: 25,
    }, 
    modal_default_out_view:{
        zIndex:20,
        width:'100%',
        height:'85vh',
        position:'absolute',
        display:'flex',
        flexDirection: 'column',
        alignItems:'center',
        justifyContent:'center' ,
        backgroundColor:'rgba(0,0,0,0)',
    },
    modal_default_inside_view:{
        zIndex:20,
        width:'98%',
        height:'87vh',
        position:'absolute',
        display:'flex',
        alignItems:'center',
        justifyContent:'center',
        paddingBottom:100,
        backgroundColor:'rgba(0,0,0,0.7)',
    },
    modal_default_input:{
        display:'flex',
        flexDirection: 'row',
        alignItems:'center',
        justifyContent:'center',
    }
}

export default primaryTheme;