import React from 'react';
import Dashboard from './pages/Dashboard';
import Login from './pages/Login';
import { BrowserRouter as Router, Route } from "react-router-dom";

export default class App extends React.Component{
  render(){
    return (
      <Router>
        <div>
          <Route exact path="/">
            <Login />
          </Route>
          <Route path="/Dashboard">
            <Dashboard />
          </Route>
        </div>
      </Router>
    );
  }
}


