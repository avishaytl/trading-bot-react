import { put ,call } from "redux-saga/effects";
import primaryValues from '../constants/Values.js'; 
import config from '../constants/Config.js'; 

const postRequest = (key, body, url = 'https://m-z-d.com/laravel/api/') =>
    Promise.race([
        fetch(url + key, {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)
          }).then(response => response.json())
          .then(responseJson => responseJson)
          .catch(error =>  console.debug('Request_error ',error)),new Promise((_, reject) => setTimeout(() => reject(new Error('timeout')), 10000))]);
// const getRequest = (key, url = 'https://m-z-d.com/laravel/api/') =>
//     Promise.race([
//         fetch(url + key).then(response => response.json())
//         .then(responseJson => responseJson)
//         .catch(error =>  console.debug('Request_error ',error)),new Promise((_, reject) => setTimeout(() => reject(new Error('timeout')), 20000))]);
const getRequestBot = ( url ) =>
    Promise.race([
        fetch(url).then(response => response.json())
        .then(responseJson => responseJson)
        .catch(error =>  console.debug('Request_error ',error)),new Promise((_, reject) => setTimeout(() => reject(new Error('timeout')), 5000))]);

export function* fetchLogin( action ) {
    let key = 'fetchLogin';
    let body = action.params;
    try{
        const response = yield call(postRequest, key, body);
        if(response.success){
            console.log('response' , response); 
            yield put({type: primaryValues.$LOGIN_RESULT, response});
        }else{
            console.log('response' , response);
            yield put({type: primaryValues.$LOGIN_ERROR, response});
        }
    }catch(e){
        console.log('fetchLogin error' + e.toString());
    }
}
export function* fetchAlphaData( action ) {
    action.params.apikey = config.company_api;
    let body = buildKeysGetRequest(config.company_api,action.params); 
    let data = [];
    try{
        const response = yield call(getRequestBot,body); 
        console.log(action.params.pos);
        data = {
            symbol: response['Global Quote']["01. symbol"],
            open: response['Global Quote']["02. open"],
            high: response['Global Quote']["03. high"],
            low: response['Global Quote']["04. low"],
            price: response['Global Quote']["05. price"],
            volume: response['Global Quote']["06. volume"],
            latest_trading_day: response['Global Quote']["07. latest trading day"],
            close: response['Global Quote']["08. previous close"],
            change: response['Global Quote']["09. change"],
            change_percent: response['Global Quote']["10. change percent"],
        }  
        switch(action.params.pos){
            case 1:
                yield put({type: primaryValues.$ALPHA_RESULT_COL1, data});
                break;
            case 2:
                yield put({type: primaryValues.$ALPHA_RESULT_COL2, data});
                break;
            case 3:
                yield put({type: primaryValues.$ALPHA_RESULT_COL3, data});
                break;
            case 4:
                yield put({type: primaryValues.$ALPHA_RESULT_COL4, data});
                break;
            case 5:
                yield put({type: primaryValues.$ALPHA_RESULT_COL5, data});
                break;
        }
    }catch(e){ 
        yield put({type: primaryValues.$ALPHA_ERROR});
        console.log('fetchAlphaData error' + e.toString());
    }
    
}
function buildKeysGetRequest(key, params){
    let keys = key;
    let startFlag = false;
    for (let key in params) { 
        if (params.hasOwnProperty(key)) {
            if(!startFlag){
                keys += '?';
                startFlag = true;
            }else
                keys += '&';
            keys += key + '=' + params[key];
        }
    } 
    return keys;
}