import { takeLatest } from "redux-saga/effects";
import  * as sagas from "./sagas.js";
import primaryValues from '../constants/Values.js';
export function* rootSaga() {
  yield takeLatest(primaryValues.$GET_LOGIN, sagas.fetchLogin); 
  yield takeLatest(primaryValues.$GET_ALPHA, sagas.fetchAlphaData); 
}